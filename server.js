var express = require('express');
var bodyParser = require('body-parser');
var port = process.env.PORT || 3000;

var app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

var mysql = require('mysql');

const mc = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'rootroot123',
    database: 'mydb'
});

mc.connect();

app.listen(port);

console.log('API server started on: ' + port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var userRoute = require('./app/routes/user');
var ticketRoute = require('./app/routes/ticket');
var messageRoute = require('./app/routes/message');
var attachmentRoute = require('./app/routes/attachment');
var siteRoute = require('./app/routes/site');
var siteHourRoute = require('./app/routes/siteHour');
var subscriptionTypeRoute = require('./app/routes/subscriptionType');
var subscriptionRoute = require('./app/routes/subscription');
var materialRoute = require('./app/routes/material');
var orderRoute = require('./app/routes/order');
var reservationRoute = require('./app/routes/reservation');

userRoute(app);
ticketRoute(app);
messageRoute(app);
attachmentRoute(app);
siteRoute(app);
siteHourRoute(app);
subscriptionTypeRoute(app);
subscriptionRoute(app);
materialRoute(app);
orderRoute(app);
reservationRoute(app);
