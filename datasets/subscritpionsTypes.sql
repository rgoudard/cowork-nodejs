INSERT INTO `subscriptiontype`(`name`, `description`, `hourPrice`, `dayPrice`, `studentPrice`, `created_at`)
VALUES ("Sans Abonnement","Payez le temps passé sur place, les consommations sont incluses et à volonté! Accessible sans réservation ou abonnement.",5,24,20,curdate());
INSERT INTO `subscriptiontype`(`name`, `description`, `hourPrice`, `dayPrice`, `subscriptionPrice`, `agreementPrice`, `agreementTime`, `created_at`)
VALUES ("Abonnement Simple","Rejoignez la communauté CO'WORK et bénéficiez de tarifs préférentiels!",4,20,24,20,12,curdate());
INSERT INTO `subscriptiontype`(`name`, `description`, `subscriptionPrice`, `agreementPrice`, `agreementTime`, `created_at`)
VALUES ("Abonnement Résident","Rejoignez la communauté CO'WORK et devenez membre résident! Bénéficiez d''un accès en illimité 7/7j",300,252,8,curdate());

