INSERT INTO `site`(`name`, `address`, `hightWiFi`, `trays`, `illimitedCons`, `created_at`) VALUES ("Co'Work Bastille","1 place de la Bastille, Paris 75010",true,true,true,curdate());
INSERT INTO `site`(`name`, `address`, `hightWiFi`, `trays`, `illimitedCons`, `created_at`) VALUES ("Co'Work Odéon","1 place d'Odéon, Paris 75010",true,true,true,curdate());
INSERT INTO `site`(`name`, `address`, `hightWiFi`, `trays`, `illimitedCons`, `created_at`) VALUES ("Co'Work République","1 place de la République, Paris 75010",true,false,true,curdate());
INSERT INTO `site`(`name`, `address`, `hightWiFi`, `trays`, `illimitedCons`, `created_at`) VALUES ("Co'Work place d'Italie","1 place d'Italie, Paris 75010",true,true,true,curdate());
INSERT INTO `site`(`name`, `address`, `hightWiFi`, `trays`, `illimitedCons`, `created_at`) VALUES ("Co'Work Ternes","1 place de Ternes, Paris 75010",true,false,true,curdate());
INSERT INTO `site`(`name`, `address`, `hightWiFi`, `trays`, `illimitedCons`, `created_at`) VALUES ("Co'Work Beaubourg","1 place de Beaubourg, Paris 75010",true,true,true,curdate());
