'user strict';
var connection = require('../../db');

var Message = function(message){
    this.message = message.message;
    this.idUser = message.idUser;
    this.idTicket = message.idTicket;
    this.date = new Date();
};

Message.createMessage = function (message, result) {
    connection.query("INSERT INTO message set ?", message, function (err, res) {

        if(err) {
            result(err, null);
        }
        else{
            result(null, res.insertId);
        }
    });
};

Message.getMessageByTicket = function(ticketId, result) {
    connection.query("SELECT * FROM message WHERE idTicket = ? ORDER BY date", ticketId , function (err, res ) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

module.exports = Message;
