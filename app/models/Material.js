'user strict';
var connection = require('../../db');

var Material = function(material){
    this.type = material.type;
    this.name = material.name;
    this.reservable = material.reservable;
    this.created_at = new Date();
    this.idSite = material.idSite;
    this.defect = material.defect;
};

Material.createMaterial = function (material, result) {
    connection.query("INSERT INTO material set ?", material, function (err, res) {

        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

Material.getAllMaterial = function(result) {
    connection.query("SELECT * FROM material WHERE deleted_at is null" , function (err, res ) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

Material.getMaterialBySite = function(siteId, result) {
    connection.query("SELECT * FROM material WHERE idSite = ? AND deleted_at is null" , siteId, function (err, res ) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

Material.updateMaterialById = function(materialId , materialUpdated, result){
    connection.query("UPDATE material SET ? WHERE id = ?", [materialUpdated, materialId], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};

Material.removeMaterial = function(name, idSite, result){
    var deletedDate = new Date();
    connection.query("UPDATE material SET deleted_at = ? WHERE idSite = ? AND name = ? AND deleted_at is null", [deletedDate, idSite, name], function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

Material.getFreeReservableMaterial = function(siteId, start, end, result) {
    connection.query("SELECT m.* FROM material m WHERE m.idSite = ? AND m.reservable = true AND m.deleted_at is null " +
        "AND NOT EXISTS ( SELECT 1 FROM reservation r WHERE r.idMaterial = m.id  AND " +
        "(r.startDate <= ? AND endDate > ? " +
        "OR r.startDate < ? AND endDate >= ? " +
        "OR r.startDate <= ? AND endDate >= ? " +
        "OR r.startDate > ? AND endDate < ? " +
        "OR r.startDate = ? AND endDate = ?))"
        , [siteId,start,start,end,end,start,end,start,end,start,end], function(err, res){
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

Material.getMaterialById = function(materialId, result) {
    connection.query("SELECT * FROM material WHERE id = ?", materialId, function (err, res) {
        if(err){
            result(err, null);
        } else {
            result(null, res);
        }
    })
};

module.exports = Material;
