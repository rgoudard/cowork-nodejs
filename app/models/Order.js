'user strict';
var connection = require('../../db');

var Order = function(order){
    this.nbTrays = order.nbTrays;
    this.date = new Date(order.date);
    this.idUser = order.idUser;
    this.idSite = order.idSite;
};

Order.createOrder= function (newOrder, result) {
    connection.query("INSERT INTO orders set ?", newOrder, function (err, res) {

        if(err) {
            result(err, null);
        }
        else{
            result(null, res.insertId);
        }
    });
};

Order.getAllOrder = function (result) {
    let date = new Date();
    connection.query("Select * from orders", date, function (err, res) {

        if(err) {
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};

Order.getOrderByUser = function (idUser, result) {

    let date = new Date();
    connection.query("SELECT * from orders where idUser = ? AND date > ? ORDER BY date " , [idUser,date], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('users : ', res);
            result(null, res);
        }
    });
};

Order.getOrderBySite = function (idSite, result) {

    let date = new Date();
    connection.query("SELECT * from orders where idSite = ? AND date > ? " , [idSite,date], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('users : ', res);
            result(null, res);
        }
    });
};


module.exports = Order;
