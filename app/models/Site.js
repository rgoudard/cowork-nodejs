'user strict';
var connection = require('../../db');

var Site = function(site){
    this.name = site.name;
    this.address = site.address;
    this.hightWiFi = site.hightWiFi;
    this.trays = site.trays;
    this.illimitedCons = site.illimitedCons;
    this.created_at = new Date();
};

Site.createSite = function (site, result) {
    connection.query("INSERT INTO site set ?", site, function (err, res) {

        if(err) {
            result(err, null);
        }
        else{
            result(null, res.insertId);
        }
    });
};

Site.getAllSites = function (result) {
    connection.query("SELECT * FROM site WHERE deleted_at is null", function (err, res){
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

Site.updateSite = function (site, siteId, result) {
    connection.query("UPDATE site SET ? WHERE id = ?", [site, siteId], function (err, res) {
        if(err) {
            result(err, null);
        }
        else {
            result(null, res);
        }
    });
};

Site.removeSite = function(siteId, result){
    var deletedDate = new Date();
    connection.query("UPDATE site SET deleted_at = ? WHERE id = ?", [deletedDate, siteId], function (err, res) {
        if(err) {
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};

Site.getSiteById = function(siteId, result) {
    connection.query("SELECT * FROM site WHERE id = ?", siteId, function(err, res) {
        if(err){
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

module.exports = Site;
