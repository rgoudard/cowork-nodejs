'user strict';
var connection = require('../../db');

var Attachment = function(attachment){
    this.name = attachment.name;
    this.attachment = attachment.attachment;
    this.idUser = attachment.idUser;
    this.idTicket = attachment.idTicket;
    this.date = new Date();
};

Attachment.createAttachment = function (attachment, result) {
    connection.query("INSERT INTO attachment set ?", attachment, function (err, res) {

        if(err) {
            result(err, null);
        }
        else{
            result(null, res.insertId);
        }
    });
};

Attachment.getAttachmentByTicket = function(ticketId, result) {
    connection.query("SELECT * FROM attachment WHERE idTicket = ? ORDER BY date", ticketId , function (err, res ) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

module.exports = Attachment;
