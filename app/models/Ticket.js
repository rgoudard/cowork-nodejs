'user strict';
var connection = require('../../db');

var Ticket = function(ticket){
    this.title = ticket.title;
    this.status = "EN ATTENTE D\'AFFECTATION";
    this.subject = ticket.subject;
    this.idUser = ticket.idUser;
    this.created_at = new Date();
    this.type = ticket.type;
    this.idMaterial = ticket.idMaterial;
};

Ticket.createTicket = function (ticket, result) {
    connection.query("INSERT INTO ticket set ?", ticket, function (err, res) {

        if(err) {
            result(err, null);
        }
        else{
            result(null, res.insertId);
        }
    });
};

Ticket.getTicketByUser = function (userId, result) {
  connection.query("SELECT * FROM ticket WHERE idUser = ? OR idUserDev = ?", [userId,userId], function (err,res) {
      if(err) {
          result(err, null);
      }
      else {
          result(null, res)
      }
  });
};

Ticket.getAllTickets = function (result) {
    connection.query("SELECT * FROM ticket", function (err,res) {
       if(err){
           result(err,null);
       } else {
           result(null,res);
       }
    });
};

Ticket.getTicketNotAffected = function(result) {
    connection.query("SELECT * FROM ticket WHERE idUserDev is null", function(err, res){
        if (err) {
            result(err, null);
        } else {
            result(null,res);
        }
    });
};

Ticket.addDeveloper = function(idUserDev, ticketId, result){
    var str = "AFFECTER";
    connection.query("UPDATE ticket SET idUserDev = ?, status = ? WHERE id = ?", [idUserDev, str, ticketId] , function (err, res) {
        if(err){
            result(err,null);
        } else {
            result(null,res);
        }
    });
};

Ticket.closeTicket = function(ticketId, result){
  var str = "CLOS";
  connection.query("UPDATE ticket SET status = ? WHERE id = ?", [str, ticketId], function (err, res) {
     if (err) {
         result(err, null);
     } else {
         result(null, res);
     }
  });
};

Ticket.getTicketById = function (id, result) {
    connection.query("SELECT * FROM ticket WHERE id = ?", id, function (err,res) {
        if(err) {
            result(err, null);
        }
        else {
            result(null, res)
        }
    })
};

module.exports = Ticket;
