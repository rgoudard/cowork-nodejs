'user strict';
var connection = require('../../db');

var Reservation = function(reservation){
    this.startDate = new Date(reservation.startDate);
    this.endDate = new Date(reservation.endDate);
    this.idMaterial = reservation.idMaterial;
    this.idUser = reservation.idUser;

};

Reservation.createReservation= function (newReservation, result) {
    connection.query("INSERT INTO reservation set ?", newReservation, function (err, res) {

        if(err) {
            result(err, null);
        }
        else{
            result(null, res.insertId);
        }
    });
};

Reservation.getAllReservation = function (result) {
    let date = new Date();
    connection.query("Select * from reservation", date, function (err, res) {

        if(err) {
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};

Reservation.getReservationByUser = function (idUser, result) {

    let date = new Date();
    connection.query("SELECT * from reservation where idUser = ? AND startDate > ? ORDER BY startDate" , [idUser,date], function (err, res) {
        if(err) {
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};


Reservation.getReservationByMaterial = function (idMaterial, result) {

    let date = new Date();
    connection.query("SELECT * from reservation where idMaterial = ? AND startDate > ? " , [idMaterial,date], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('users : ', res);
            result(null, res);
        }
    });
};

module.exports = Reservation;
