'user strict';
var connection = require('../../db');

var Subscription = function(subscription){
    this.startDate = new Date();
    this.endDate = new Date();
    this.endDate = new Date(this.endDate.setMonth(this.endDate.getMonth() + subscription.agreementTime));
    this.idUser = subscription.idUser;
    this.idSubscriptionType = subscription.idSubscriptionType;
};

Subscription.createSubscription= function (newSubscription, result) {
    connection.query("INSERT INTO subscription set ?", newSubscription, function (err, res) {

        if(err) {
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};

Subscription.getAllSubscription = function (result) {
    connection.query("Select * from subscription", function (err, res) {

        if(err) {
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};

Subscription.getCurrentSubscription = function (result) {
            connection.query("Select * from subscription where SYSDATE() between startDate and endDate", function (err, res) {

        if(err) {
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};


Subscription.getSubscriptionByUser = function(userId, result){
    connection.query("SELECT * FROM subscription WHERE idUser = ? AND SYSDATE() between startDate and endDate", userId, function (err,res) {
        if(err) {
            result(err, null);
        }
        else {
            result(null, res)
        }
    });
};

module.exports = Subscription;
