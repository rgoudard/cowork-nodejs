'user strict';
var connection = require('../../db');

var SiteHour = function(siteHour){
    this.day = siteHour.day;
    this.open = siteHour.open;
    this.close = siteHour.close;
    this.idSite = siteHour.idSite;

};

SiteHour.addHourToSite = function (siteHour, result) {
    connection.query("INSERT INTO sitehour set ?", siteHour, function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res.insertId);
        }
    });
};

SiteHour.updateHour = function (siteHourId, newSiteHour, result) {
    connection.query("UPDATE sitehour SET ? WHERE id = ?", [newSiteHour,siteHourId], function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

SiteHour.getAllHoursBySite = function (siteId, result) {
    connection.query("SELECT * FROM sitehour WHERE idSite = ?", siteId, function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

SiteHour.getAllHours = function (result) {
    connection.query("SELECT * FROM sitehour", function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

module.exports = SiteHour;
