'user strict';
var connection = require('../../db');

var SubscriptionType = function(subscriptionType){
    this.name = subscriptionType.name;
    this.price = subscriptionType.price;
    this.agreementPrice = subscriptionType.agreementPrice;
    this.agreementTime = subscriptionType.agreementTime;
    this.created_at = new Date();
};

SubscriptionType.createSubscriptionType= function (newSubscritpionType, result) {
    connection.query("INSERT INTO subscriptiontype set ?", newSubscritpionType, function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};

SubscriptionType.getAllSubscriptionType = function (result) {
    connection.query("Select * from subscriptiontype where deleted_at is null", function (err, res) {

        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('users : ', res);

            result(null, res);
        }
    });
};

SubscriptionType.updateSubscriptionTypeById = function(subscriptionTypeId , subscriptionTypeUpdated, result){
    connection.query("UPDATE subscriptiontype SET ? WHERE id = ?", [subscriptionTypeUpdated, subscriptionTypeId], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};

SubscriptionType.removeSubscriptionType = function(subscriptionTypeId, result){
    var deletedDate = new Date();
    connection.query("UPDATE subscriptionType SET deleted_at = ? WHERE id = ?", [deletedDate, subscriptionTypeId], function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

SubscriptionType.getSubscriptionTypeById = function(subscriptionTypeId, result) {
    connection.query("SELECT * FROM subscriptiontype WHERE id = ? ", subscriptionTypeId, function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

module.exports = SubscriptionType;
