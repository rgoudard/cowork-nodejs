'user strict';
var connection = require('../../db');

var User = function(user){
    this.firstname = user.firstname;
    this.lastname = user.lastname;
    this.email = user.email;
    this.password = user.password;
    this.type = "USER";
    this.created_at = new Date();
};
User.createUser= function (newUser, result) {
    connection.query("INSERT INTO user set ?", newUser, function (err, res) {

        if(err) {
            result(err, null);
        }
        else{
            result(null, res.insertId);
        }
    });
};
User.getUserById = function (userId, result) {
    connection.query("Select * from user where id = ?" , userId, function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);

        }
    });
};
User.getAllUsers = function (result) {
    connection.query("Select * from user where deleted_at is null", function (err, res) {

        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};
User.updateUserById = function(userId , lastname, firstname, email, result){
    connection.query("UPDATE user SET lastname = ? , firstname = ? , email = ? WHERE id = ?", [lastname, firstname, email, userId], function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

User.updatePassword = function(userId,password, result) {

    connection.query("UPDATE user SET password = ? WHERE id = ?", [password,userId], function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    })
};

User.updateType = function(userId, type, result) {
    connection.query("UPDATE user SET type = ? WHERE id = ?", [type, userId], function (err, res ) {
        if (err) {
            reslut(err, null);
        } else {
            result(null, res);
        }
    })
};

User.removeUser = function(userId, result){
    var deletedDate = new Date();
    connection.query("UPDATE user SET deleted_at = ? WHERE id = ?", [deletedDate, userId], function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};

User.connection = function (email, password, result){
  connection.query("SELECT * FROM user WHERE email = ? AND password = ? AND deleted_at is null", [email,password], function (err, res) {

      if(err) {
          result(err, null);
      }
      else if (res.length === 0 ) {
          err = 'No match found';
          result(err,null);
      }
      else {
          result(null, res);
      }
  });
};

module.exports = User;
