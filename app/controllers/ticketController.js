'use strict';

var Ticket = require('../models/Ticket');


exports.createTicket = function(req, res) {
    var ticket = new Ticket(req.body);

    Ticket.createTicket(ticket, function(err, ticket) {
        if (err)
            res.send(err);
        else
            res.json({
                ticket,
                success: true
            });
    });
};

exports.getTicketByUser = function (req, res) {
    Ticket.getTicketByUser(req.params.userId, function(err, tickets) {
        if(err){
            res.send(err);
        }else{
            res.json({
                tickets,
            });
        }
    });
};

exports.getAllTickets = function (req, res){
    Ticket.getAllTickets(function (err,ticket) {
       if(err){
           res.send(err);
       } else {
           res.json({
               ticket
           });
       }
    });
};


exports.getTicketNotAffected = function (req, res) {
    Ticket.getTicketNotAffected ( function (err, tickets) {
        if (err) {
            res.send(err);
        } else {
            res.json({
                tickets
            });
        }
    });
}

exports.addDeveloper = function (req, res) {
    Ticket.addDeveloper(req.body.idUserDev , req.params.ticketId , function (err, ticket) {
        if(err){
            res.send(err);
        } else {
            res.json({
                ticket,
                success: true
            });
        }
    });
};

exports.closeTicket = function (req, res) {
    Ticket.closeTicket(req.params.ticketId, function (err, ticket){
        if(err) {
            res.send(err);
        } else {
            res.json({
                ticket,
                success: true
            });
        }
    });
};

exports.getTicketById = function (req, res) {
    Ticket.getTicketById(req.params.ticketId, function(err, ticket) {
        if(err){
            res.send(err);
        }else{
            res.json({
                ticket,
            });
        }
    });
};
