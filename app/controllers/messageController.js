'use strict';

var Message = require('../models/Message');


exports.createMessage = function(req, res) {
    var message = new Message(req.body);

    Message.createMessage(message, function(err, message) {
        if (err)
            res.send(err);
        else
            res.json({
                message,
                success: true
            });
    });
};

exports.getMessageByTicket = function (req, res) {
    Message.getMessageByTicket(req.params.ticketId, function (err, messages) {
        if (err)
            res.send(err);
        else
            res.json({
                messages
            });
    })  
};
