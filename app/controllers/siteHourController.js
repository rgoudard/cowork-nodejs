'use strict';

var SiteHour = require('../models/SiteHour');


exports.addHourToSite = function(req, res) {
    var siteHour = new SiteHour (req.body);

    SiteHour.addHourToSite(siteHour, function(err, siteHour) {
        if (err)
            res.send(err);
        else
            res.json({
                siteHour,
                success: true
            });
    });
};

exports.getAllHours = function(req, res) {
    SiteHour.getAllHours(function(err, siteHours) {
        if (err)
            res.send(err);
        else
            res.json({
                siteHours
            })
    })
}

exports.updateHour = function(req, res) {
    var newSiteHour = new SiteHour(req.body);

    SiteHour.updateHour(req.params.hourId, newSiteHour , function (err, site) {
        if (err)
            res.send(err);
        res.json(site);
    });
};

exports.getAllHoursBySite = function(req, res) {
    SiteHour.getAllHoursBySite(req.params.idSite, function (err, siteHours) {
        if (err)
            res.send(err);
        res.json({siteHours});
    })
};
