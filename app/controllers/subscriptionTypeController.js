'use strict';

var SubscriptionType = require('../models/SubscriptionType');

exports.getAllSubscriptionType = function(req, res) {
    SubscriptionType.getAllSubscriptionType(function(err, subscriptionType) {
        if (err)
            res.send(err);
        else
            res.json({subscriptionType});
    });
};

exports.getSubscriptionTypeById = function(req, res) {
    SubscriptionType.getSubscriptionTypeById(req.params.subscriptionTypeId, function(err, subscriptionType) {
        if (err)
            res.send(err);
        else
            res.json({subscriptionType});
    });
};

exports.createSubscriptionType = function(req, res) {
    var newSubscriptionType = new SubscriptionType(req.body);

    SubscriptionType.createSubscriptionType(newSubscriptionType, function(err, subscriptionType) {

        if (err)
            res.send(err);
        res.json(subscriptionType);
    });
};

exports.updateSubscriptionType = function(req, res) {
    SubscriptionType.updateSubscriptionTypeById(req.params.subscriptionTypeId, new SubscriptionType(req.body), function(err, subscriptionType) {
        if (err)
            res.send(err);
        res.json(subscriptionType);
    });
};


exports.deleteSubscriptionType = function(req, res) {


    SubscriptionType.removeSubscriptionType( req.params.subscriptionTypeId, function(err, subscriptionType) {
        if (err)
            res.send(err);
        res.json({ message: subscriptionType.name + ': SubscriptionType successfully deleted' });
    });
};
