'use strict';

var Subscription = require('../models/Subscription');

exports.getAllSubscription = function(req, res) {
    Subscription.getAllSubscription(function(err, subscription) {
        if (err) {
            res.send(err);
        } else {
            res.json({
                subscription
            })
        }
    });
};

exports.createSubscription = function(req, res) {
    var newSubscription = new Subscription(req.body);

    Subscription.createSubscription(newSubscription, function(err, subscription) {

        if (err)
            res.send(err);
        else
            res.json({
                subscription,
                success:true
            });
    });
};

exports.getCurrentSubscription = function(req, res) {
    Subscription.getCurrentSubscription(function(err, subscription) {
        if (err) {
            res.send(err);
        } else {
            res.json({
                subscription
            })
        }
    });
};

exports.getSubscriptionByUser = function(req, res) {
    Subscription.getSubscriptionByUser(req.params.userId, function(err, subscription) {
        if(err){
            res.send(err);
        }else{
            res.json({
                subscription,
            });
        }
    });
};

