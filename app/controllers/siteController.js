'use strict';

var Site = require('../models/Site');


exports.createSite = function(req, res) {
    var site = new Site(req.body);

    Site.createSite(site, function(err, site) {
        if (err)
            res.send(err);
        else
            res.json({
                site,
                success: true
            });
    });
};

exports.getAllSites = function(req, res) {
    Site.getAllSites(function(err,sites){
        if(err){
            res.send(err);
        }else{
            res.json({sites});
        }
    });
};

exports.getSiteById = function(req, res) {
    Site.getSiteById( req.params.siteId, function(err, site) {
       if(err) {
           res.send(err);
       } else {
           res.json({
               site,
               success: true
           });
       }
    });
};

exports.updateSite = function (req, res) {
    var site = new Site(req.body);

    Site.updateSite(site, req.params.siteId, function (err, site) {
        if(err){
            res.send(err);
        }else{
            res.json(site);
        }
    });
};

exports.removeSite = function (req, res) {
  Site.removeSite(req.params.siteId, function (err, site){
      if(err){
          res.send(err);
      }else{
          res.json({site,
              success: true});
      }
  });
};
