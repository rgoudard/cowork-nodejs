'use strict';

var Attachment = require('../models/Attachment');

exports.createAttachment = function (req, res){
    var attachment = new Attachment(req.body);

    Attachment.createAttachment(attachment, function(err, attachment) {
        if (err)
            res.send(err);
        else
            res.json({
                attachment,
                success: true
            });
    });
};

exports.getAttachmentByTicket = function (req, res) {
    Attachment.getAttachmentByTicket(req.params.ticketId, function (err, attachments) {
        if (err)
            res.send(err);
        else
            res.json({attachments});
    })
};
