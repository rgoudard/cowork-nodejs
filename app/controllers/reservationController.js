'use strict';

var Reservation = require('../models/Reservation');

exports.getAllReservation = function(req, res) {
    Reservation.getAllReservation(function(err, reservations) {
        if (err)
            res.send(err);
        else
            res.json({
                reservations,
                'success' : true,
                'length' : reservations.length
            });
    });
};

exports.createReservation = function(req, res) {
    var newReservation = new Reservation(req.body);

    Reservation.createReservation(newReservation, function(err, reservation) {

        if (err)
            res.send(err);
        res.json({
            reservation,
            'success': true
        });
    });
};

exports.getReservationByMaterial = function(req, res) {
    var idMaterial = req.params.idMaterial;

    Reservation.getReservationByMaterial(idMaterial ,function(err, reservations) {
        if (err)
            res.send(err);
        else
            res.json({
                reservations,
                'success' : true,
                'length' : reservations.length
            });
    });
};


exports.getReservationByUser = function(req, res) {
    var idUser = req.params.idUser;

    Reservation.getReservationByUser(idUser ,function(err, reservations) {
        if (err)
            res.send(err);
        else
            res.json({
                reservations,
                'success' : true,
                'length' : reservations.length
            });
    });
};

