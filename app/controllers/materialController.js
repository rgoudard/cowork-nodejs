'use strict';

var Material = require('../models/Material');

exports.createMaterial = function (req, res){
    var material = new Material(req.body);

    Material.createMaterial(material, function(err, material) {
        if (err) {
            res.send(err);
        } else {
            res.json({material, success: true});
        }
    });
};

exports.getAllMaterial = function (req, res) {
    Material.getAllMaterial(function (err, material) {
        if (err)
            res.send(err);
        res.json({
            material
        });
    })
};

exports.getMaterialBySite = function (req, res) {
    Material.getMaterialBySite(req.params.siteId, function (err, material) {
        if (err)
        {
            res.send(err);
        }
        else {
            res.json({material});
        }
    });
};

exports.updateMaterial = function(req, res) {
    Material.updateMaterialById(req.params.materialId, new Material(req.body), function(err, material) {
        if (err)
        {
            res.send(err);
        }
        else {
            res.json({material});
        }
    });
};


exports.deleteMaterial = function(req, res) {

    Material.removeMaterial( req.body.name, req.body.idSite, function(err, material) {
        if (err){
            res.send(err);
        } else {
            res.json( material );
        }
    });
};


exports.getFreeReservableMaterial = function (req, res) {
    let start = req.body.start;
    let end = req.body.end;
    Material.getFreeReservableMaterial(req.params.siteId, start, end,function(err, materials) {
        if (err)
            res.send(err);
        else res.json({materials});
    });
};

exports.getMaterialById = function (req, res) {
    Material.getMaterialById(req.params.materialId, function(err, material) {
        if (err)
            res.send(err);
        else res.json({material});
    });
}
