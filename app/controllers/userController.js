'use strict';

var User = require('../models/User');

exports.getAllUsers = function(req, res) {
    User.getAllUsers(function(err, users) {
        if (err)
            res.send(err);
        else
            res.json({
                users
            });
    });
};



exports.createUser = function(req, res) {
    var newUser = new User(req.body);
    User.createUser(newUser, function(err, user) {

        if (err)
            res.send(err);
        else
            res.json({
                user,
                success: true
            });
    });
};


exports.getUser = function(req, res) {
    User.getUserById(req.params.userId, function(err, user) {
        if (err)
            res.send(err);
        res.json({
            user,
            'success': true
        });
    });
};


exports.updateUser = function(req, res) {
    User.updateUserById(req.params.userId, req.body.lastname, req.body.firstname, req.body.email, function(err, user) {
        if (err)
            res.send(err);
        res.json({
            user,
            "success":true
        });
    });
};

exports.updateType = function(req, res){
    User.updateType(req.params.userId, req.body.type, function (err, user) {
       if (err)
           res.send(err);
       else {
           res.json({
               user,
               success: true
           });
       }
    });
};

exports.updatePassword = function(req, res){
    User.updatePassword(req.params.userId, req.body.password, function (err, user) {
        if (err)
            res.send(err);
        res.json({
            user,
            "success":true
        });
    });
};

exports.deleteUser = function(req, res) {


    User.removeUser( req.params.userId, function(err, user) {
        if (err)
            res.send(err);
        res.json({'success': true});
    });
};

exports.connection = function(req, res) {
    User.connection(req.body.email, req.body.password, function(err, user) {
        if (err)
            res.json({
                err,
                'success':false
            });
        else
            res.json( {
                user,
                "success":true
            })
    });

};
