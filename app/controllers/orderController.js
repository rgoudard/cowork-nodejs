'use strict';

var Order = require('../models/Order');

exports.getAllOrder = function(req, res) {
    Order.getAllOrder(function(err, orders) {
        if (err)
            res.send(err);
        res.send({
            orders,
            'success' : true,
            'length' : orders.length
        });
    });
};

exports.createOrder = function(req, res) {
    var newOrder = new Order(req.body);

    Order.createOrder(newOrder, function(err, order) {

        if (err)
        {
            res.send(err);
        } else {
            res.json({order,
                success: true
            });
        }
    });
};

exports.getOrderBySite = function(req, res) {
    var idSite = req.params.idSite;

    Order.getOrderBySite(idSite ,function(err, order) {
        if (err)
            res.send(err);
        res.send(order);
    });
};


exports.getOrderByUser = function(req, res) {
    var idUser = req.params.idUser;

    Order.getOrderByUser(idUser ,function(err, orders) {
        if (err) {
            res.send(err);
        } else {
            res.send({orders});
        }
    });
};

