'use strict';
module.exports = function(app) {
    var ticketController = require('../controllers/ticketController');

    app.route('/tickets')
        .post(ticketController.createTicket)
        .get(ticketController.getAllTickets);

    app.route('/ticketNotAffected')
        .get(ticketController.getTicketNotAffected);

    app.route('/ticket/:ticketId')
        .get(ticketController.getTicketById);

    app.route('/tickets/:userId')
        .get(ticketController.getTicketByUser);

    app.route('/tickets/:ticketId')
        .put(ticketController.addDeveloper);

    app.route('/closeTicket/:ticketId')
        .put(ticketController.closeTicket);

};
