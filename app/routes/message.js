'use strict';
module.exports = function(app) {
    var messageController = require('../controllers/messageController');

    app.route('/messages')
        .post(messageController.createMessage);

    app.route('/messages/:ticketId')
        .get(messageController.getMessageByTicket);
};
