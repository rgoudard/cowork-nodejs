'use strict';
module.exports = function(app) {
    var userController = require('../controllers/userController');

    app.route('/users')
        .get(userController.getAllUsers)
        .post(userController.createUser);

    app.route('/users/:userId')
        .get(userController.getUser)
        .put(userController.updateUser)
        .delete(userController.deleteUser);

    app.route('/manageUserType/:userId')
        .put(userController.updateType);

    app.route('/user/:userId')
        .put(userController.updatePassword);

    app.route('/connection')
        .post(userController.connection);
};
