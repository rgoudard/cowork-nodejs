'use strict';
module.exports = function(app) {
    var materialController = require('../controllers/materialController');

    app.route('/material')
        .post(materialController.createMaterial)
        .get(materialController.getAllMaterial)
        .delete(materialController.deleteMaterial)
    ;

    app.route('/material/site/:siteId')
        .get(materialController.getMaterialBySite);

    app.route('/material/:materialId')
        .put(materialController.updateMaterial)
        .get(materialController.getMaterialById);

    app.route('/freeReservableMaterial/:siteId')
        .post(materialController.getFreeReservableMaterial);
};
