'use strict';
module.exports = function(app) {
    var siteController = require('../controllers/siteController');

    app.route('/sites')
        .post(siteController.createSite)
        .get(siteController.getAllSites);

    app.route('/sites/:siteId')
        .get(siteController.getSiteById)
        .put(siteController.updateSite)
        .delete(siteController.removeSite);
};
