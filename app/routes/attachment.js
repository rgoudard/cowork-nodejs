'use strict';
module.exports = function(app) {
    var attachmentController = require('../controllers/attachmentController');

    app.route('/attachments')
        .post(attachmentController.createAttachment);

    app.route('/attachments/:ticketId')
        .get(attachmentController.getAttachmentByTicket);
};
