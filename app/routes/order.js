'use strict';
module.exports = function(app) {
    var orderController = require('../controllers/orderController');

    app.route('/order')
        .get(orderController.getAllOrder)
        .post(orderController.createOrder);

    app.route('/order/site/:idSite')
        .get(orderController.getOrderBySite);
    app.route('/order/user/:idUser')
        .get(orderController.getOrderByUser);
};
