'use strict';
module.exports = function(app) {
    var reservationController = require('../controllers/reservationController');

    app.route('/reservation')
        .get(reservationController.getAllReservation)
        .post(reservationController.createReservation);

    app.route('/reservation/site/:idMaterial')
        .get(reservationController.getReservationByMaterial);
    app.route('/reservation/user/:idUser')
        .get(reservationController.getReservationByUser);
};
