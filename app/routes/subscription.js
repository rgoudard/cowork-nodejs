'use strict';
module.exports = function(app) {
    var subscriptionController = require('../controllers/subscriptionController');

    app.route('/subscription')
        .get(subscriptionController.getAllSubscription)
        .post(subscriptionController.createSubscription);

    app.route('/currentSubscription')
        .get(subscriptionController.getCurrentSubscription);

    app.route('/subscription/:userId')
        .get(subscriptionController.getSubscriptionByUser);
};
