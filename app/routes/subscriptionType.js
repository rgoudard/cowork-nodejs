'use strict';
module.exports = function(app) {
    var subscriptionTypeController = require('../controllers/subscriptionTypeController');

    app.route('/subscriptionType')
        .get(subscriptionTypeController.getAllSubscriptionType)
        .post(subscriptionTypeController.createSubscriptionType);

    app.route('/subscriptionType/:subscriptionTypeId')
        .get(subscriptionTypeController.getSubscriptionTypeById)
        .put(subscriptionTypeController.updateSubscriptionType)
        .delete(subscriptionTypeController.deleteSubscriptionType);

};
