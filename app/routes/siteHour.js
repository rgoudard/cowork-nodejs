'use strict';
module.exports = function(app) {
    var siteHourController = require('../controllers/siteHourController');

    app.route('/siteHours')
        .post(siteHourController.addHourToSite)
        .get(siteHourController.getAllHours);

    app.route('/siteHours/:idSite')
        .get(siteHourController.getAllHoursBySite);


    app.route('/siteHours/:hourId')
        .put(siteHourController.updateHour);
};


